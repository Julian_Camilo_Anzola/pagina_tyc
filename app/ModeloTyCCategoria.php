<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModeloTyCCategoria extends Model
{
    
	protected $table ='tyc_categoria';

    protected $fillable =['id','nombres','rutas_imagenes','ceudonimosUbicaciones'];

    public function tyc_subcategoria(){

    	return $this->belongsTo('App\tyc_subcategoria');

    }

}
