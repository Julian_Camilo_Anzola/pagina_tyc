<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MigracionTyCSubCategoria extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tyc_subcategoria', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->string('nombres')->nullable($value = false)->collation('utf8_spanish2_ci');
            $table->string('rutas_imagenes')->nullable($value=true)->collation('utf8_spanish2_ci');
            $table->bigInteger('fid_categorias')->nullable($value=false)->unsigned();
            $table->foreign('fid_categorias')->references('id')->on('tyc_categoria')->onDelete('cascade');
            $table->string('ceudonimosUbicaciones')->nullable($value=false)->collation('utf8_spanish2_ci');
            $table->timestamps();
        });

        DB::table('tyc_subcategoria')->insert(
            array(
                'nombres' => 'ILUMINACION',
                'rutas_imagenes' => 'No hay ruta',
                'fid_categorias' => '1',
                'ceudonimosUbicaciones' => 'CO',
                'id' => '1'
            )
        );
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tyc_subcategoria');
    }
}
