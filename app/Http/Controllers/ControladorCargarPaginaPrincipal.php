<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\ModeloTyCCategoria;

use App\ModeloTyCSubCategoria;

use App\ModeloTyCProducto;

use App\ModeloTyCInformacionesEspecificas;

use Location;

class ControladorCargarPaginaPrincipal extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        session_start();

        $ipaddress = $_SERVER['REMOTE_ADDR'];

        $position = Location::get($ipaddress);

        session()->put('ubicacion', $position->countryCode);

        if($position->countryCode=="EC"){

            $modeloTyCCategoriaBusqueda=ModeloTyCCategoria::where('ceudonimosUbicaciones',"EC")->get();

                $modeloTyCSubCategoria=ModeloTyCSubCategoria::where('ceudonimosUbicaciones',"EC")->get();

                $modeloTyCCategoriaBusqueda2=ModeloTyCCategoria::where('nombres',"PROMOCION")->get()->first();

                $modeloModeloTyCInformacionesEspecificas=ModeloTyCInformacionesEspecificas::where('idCategoria',$modeloTyCCategoriaBusqueda2->id)->get();

                $numero=Count($modeloModeloTyCInformacionesEspecificas);

                return view('VistaPrincipalEcuador',compact('modeloTyCCategoriaBusqueda','modeloTyCSubCategoria','numero','modeloModeloTyCInformacionesEspecificas'));

        }else{

                $modeloTyCCategoriaBusqueda=ModeloTyCCategoria::where('ceudonimosUbicaciones',"CO")->get();

                $modeloTyCSubCategoria=ModeloTyCSubCategoria::where('ceudonimosUbicaciones',"CO")->get();

                $modeloTyCCategoriaBusqueda2=ModeloTyCCategoria::where('nombres',"PROMOCION")->get()->first();

                $modeloModeloTyCInformacionesEspecificas=ModeloTyCInformacionesEspecificas::where('idCategoria',$modeloTyCCategoriaBusqueda2->id)->get();

                $numero=Count($modeloModeloTyCInformacionesEspecificas);

                return view('VistaPrincipal',compact('modeloTyCCategoriaBusqueda','modeloTyCSubCategoria','numero','modeloModeloTyCInformacionesEspecificas'));

        }

    }

    public function verLoginAdministrador(){

        return view('VistaLoginAdministrador');
    }

    public function verHistoria(){

        if(session('ubicacion')=="EC"){

            $modeloTyCCategoriaBusqueda=ModeloTyCCategoria::where('ceudonimosUbicaciones',"EC")->get();

            $modeloTyCSubCategoria=ModeloTyCSubCategoria::where('ceudonimosUbicaciones',"EC")->get();

            return view('VistaHistoria',compact('modeloTyCCategoriaBusqueda','modeloTyCSubCategoria'));            

        }else{

            $modeloTyCCategoriaBusqueda=ModeloTyCCategoria::where('ceudonimosUbicaciones',"CO")->get();

            $modeloTyCSubCategoria=ModeloTyCSubCategoria::where('ceudonimosUbicaciones',"CO")->get();

            return view('VistaHistoria',compact('modeloTyCCategoriaBusqueda','modeloTyCSubCategoria'));

        }

    }

    public function verContactos(){

        if(session('ubicacion')=="EC"){

            $modeloTyCCategoriaBusqueda=ModeloTyCCategoria::where('ceudonimosUbicaciones',"EC")->get();

            $modeloTyCSubCategoria=ModeloTyCSubCategoria::where('ceudonimosUbicaciones',"EC")->get();

            return view('VistaContactos',compact('modeloTyCCategoriaBusqueda','modeloTyCSubCategoria'));            

        }else{

            $modeloTyCCategoriaBusqueda=ModeloTyCCategoria::where('ceudonimosUbicaciones',"CO")->get();

            $modeloTyCSubCategoria=ModeloTyCSubCategoria::where('ceudonimosUbicaciones',"CO")->get();

            return view('VistaContactos',compact('modeloTyCCategoriaBusqueda','modeloTyCSubCategoria'));

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
