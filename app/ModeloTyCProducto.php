<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModeloTyCProducto extends Model
{
    
	protected $table ='tyc_productos';

    protected $fillable =['id','cantidades','nombres','fid_subcategorias','ubicaciones','precios','ceudonimosUbicaciones'];

    public function tyc_subcategoria(){

    	return $this->hasOne('App\tyc_subcategoria');

    }

    public function tyc_informaciones_especificas(){

    	return $this->belongsTo('App\tyc_informaciones_especificas');

    }

}
