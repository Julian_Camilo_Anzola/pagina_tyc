<?php

namespace App\Http\Controllers;

use Storage;

use Illuminate\Http\Request;

use App\ModeloTyCSubCategoria;

use App\ModeloTyCCategoria;

use App\ModeloTyCProducto;

use App\ModeloTyCInformacionesEspecificas;

use Excel;

class ControladorModificadorProducto extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        
        if(session('ubicacion')=="EC"){

            $modeloTyCCategoriaBusqueda=ModeloTyCCategoria::where('ceudonimosUbicaciones',"EC")->get();

            $modeloTyCSubCategoria=ModeloTyCSubCategoria::where('ceudonimosUbicaciones',"EC")->get();

            return view('VistaAgregarProducto',compact('modeloTyCCategoriaBusqueda','modeloTyCSubCategoria'));            

        }else{

            $modeloTyCCategoriaBusqueda=ModeloTyCCategoria::where('ceudonimosUbicaciones',"CO")->get();

            $modeloTyCSubCategoria=ModeloTyCSubCategoria::where('ceudonimosUbicaciones',"CO")->get();

            return view('VistaAgregarProducto',compact('modeloTyCCategoriaBusqueda','modeloTyCSubCategoria'));

        }
    }

    public function verImportacionMasiva(){

        if(session('ubicacion')=="EC"){

            $modeloTyCCategoriaBusqueda=ModeloTyCCategoria::where('ceudonimosUbicaciones',"EC")->get();

            $modeloTyCSubCategoria=ModeloTyCSubCategoria::where('ceudonimosUbicaciones',"EC")->get();

            return view('VistaImportarProductosMasivamente',compact('modeloTyCCategoriaBusqueda','modeloTyCSubCategoria'));            

        }else{

            $modeloTyCCategoriaBusqueda=ModeloTyCCategoria::where('ceudonimosUbicaciones',"CO")->get();

            $modeloTyCSubCategoria=ModeloTyCSubCategoria::where('ceudonimosUbicaciones',"CO")->get();

            return view('VistaImportarProductosMasivamente',compact('modeloTyCCategoriaBusqueda','modeloTyCSubCategoria'));

        }
    }

    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function agregarProducto(Request $request){

        if($request->input('ubicacion')=="Colombia"){

            $modeloTyCCategoria = ModeloTyCCategoria::where('nombres',$request->input('categoria'))->where('ceudonimosUbicaciones',"CO")->get()->first();

            if(!is_null($modeloTyCCategoria)){

                $modeloTyCSubCategoria= ModeloTyCSubCategoria::where('nombres',$request->input('subCatgoria'))->where('ceudonimosUbicaciones',"CO")->get()->first();

                if(!is_null($modeloTyCSubCategoria)){

                    $modeloTyCProducto=new ModeloTyCProducto;

                    $modeloTyCInformacionesEspecificas=new ModeloTyCInformacionesEspecificas;

                    if($request->file('UrlImg2')!=null){

                        $ruta=time().'_'.$request->file('UrlImg2')->getClientOriginalName();

                        Storage::disk('imagenProducto')->put($ruta,file_get_contents($request->file('UrlImg2')->getRealPath()));

                        $ruta1='imagenProducto/'.$ruta;

                        $modeloTyCInformacionesEspecificas->rutas_imagenes=$ruta1;

                        $modeloTyCProducto->cantidades=0;

                        $modeloTyCProducto->nombres=$request->input('nombreProducto');

                        $modeloTyCProducto->fid_subcategorias=$modeloTyCSubCategoria->id;

                        $modeloTyCProducto->ubicaciones=$request->input('ubicacion');

                        if($request->input('ubicacion')=="Colombia"){

                            $modeloTyCProducto->ceudonimosUbicaciones="CO";

                            $modeloTyCInformacionesEspecificas->ceudonimosUbicaciones="CO";

                        }

                        if($request->input('ubicacion')=="Ecuador"){

                            $modeloTyCProducto->ceudonimosUbicaciones="EC";

                            $modeloTyCInformacionesEspecificas->ceudonimosUbicaciones="EC";

                        }

                        $modeloTyCProducto->precios=0;

                        $modeloTyCProducto->save();

                        $modeloTyCInformacionesEspecificas->fid_subcategorias=$modeloTyCSubCategoria->id;

                        $modeloTyCInformacionesEspecificas->descripciones=$request->input('descripcionProducto');

                        $modeloTyCInformacionesEspecificas->codigos=$request->input('codigoProducto');

                        $modeloTyCInformacionesEspecificas->fid_productos=$modeloTyCProducto->id;

                        $modeloTyCInformacionesEspecificas->idCategoria=$modeloTyCCategoria->id;

                        $modeloTyCInformacionesEspecificas->save();

                    }else{

                        $modeloTyCInformacionesEspecificas->rutas_imagenes="imagenProducto/placeholder.png";

                        $modeloTyCProducto->cantidades=0;

                        $modeloTyCProducto->nombres=$request->input('nombreProducto');

                        $modeloTyCProducto->fid_subcategorias=$modeloTyCSubCategoria->id;

                        $modeloTyCProducto->ubicaciones=$request->input('ubicacion');

                        if($request->input('ubicacion')=="Colombia"){

                            $modeloTyCProducto->ceudonimosUbicaciones="CO";

                            $modeloTyCInformacionesEspecificas->ceudonimosUbicaciones="CO";

                        }

                        if($request->input('ubicacion')=="Ecuador"){

                            $modeloTyCProducto->ceudonimosUbicaciones="EC";

                            $modeloTyCInformacionesEspecificas->ceudonimosUbicaciones="EC";

                        }

                        $modeloTyCProducto->precios=0;

                        $modeloTyCProducto->save();

                        $modeloTyCInformacionesEspecificas->fid_subcategorias=$modeloTyCSubCategoria->id;

                        $modeloTyCInformacionesEspecificas->descripciones=$request->input('descripcionProducto');

                        $modeloTyCInformacionesEspecificas->codigos=$request->input('codigoProducto');

                        $modeloTyCInformacionesEspecificas->fid_productos=$modeloTyCProducto->id;

                        $modeloTyCInformacionesEspecificas->idCategoria=$modeloTyCCategoria->id;

                        $modeloTyCInformacionesEspecificas->save();

                    }

                }

            }

        }else{

            $modeloTyCCategoria = ModeloTyCCategoria::where('nombres',$request->input('categoria'))->where('ceudonimosUbicaciones',"EC")->get()->first();

            if(!is_null($modeloTyCCategoria)){

                $modeloTyCSubCategoria= ModeloTyCSubCategoria::where('nombres',$request->input('subCatgoria'))->where('ceudonimosUbicaciones',"EC")->get()->first();

                if(!is_null($modeloTyCSubCategoria)){

                    $modeloTyCProducto=new ModeloTyCProducto;

                    $modeloTyCInformacionesEspecificas=new ModeloTyCInformacionesEspecificas;

                    if($request->file('UrlImg2')!=null){

                        $ruta=time().'_'.$request->file('UrlImg2')->getClientOriginalName();

                        Storage::disk('imagenProducto')->put($ruta,file_get_contents($request->file('UrlImg2')->getRealPath()));

                        $ruta1='imagenProducto/'.$ruta;

                        $modeloTyCInformacionesEspecificas->rutas_imagenes=$ruta1;

                        $modeloTyCProducto->cantidades=0;

                        $modeloTyCProducto->nombres=$request->input('nombreProducto');

                        $modeloTyCProducto->fid_subcategorias=$modeloTyCSubCategoria->id;

                        $modeloTyCProducto->ubicaciones=$request->input('ubicacion');

                        if($request->input('ubicacion')=="Colombia"){

                            $modeloTyCProducto->ceudonimosUbicaciones="CO";

                            $modeloTyCInformacionesEspecificas->ceudonimosUbicaciones="CO";

                        }

                        if($request->input('ubicacion')=="Ecuador"){

                            $modeloTyCProducto->ceudonimosUbicaciones="EC";

                            $modeloTyCInformacionesEspecificas->ceudonimosUbicaciones="EC";

                        }

                        $modeloTyCProducto->precios=0;

                        $modeloTyCProducto->save();

                        $modeloTyCInformacionesEspecificas->fid_subcategorias=$modeloTyCSubCategoria->id;

                        $modeloTyCInformacionesEspecificas->descripciones=$request->input('descripcionProducto');

                        $modeloTyCInformacionesEspecificas->codigos=$request->input('codigoProducto');

                        $modeloTyCInformacionesEspecificas->fid_productos=$modeloTyCProducto->id;

                        $modeloTyCInformacionesEspecificas->idCategoria=$modeloTyCCategoria->id;

                        $modeloTyCInformacionesEspecificas->save();

                    }else{

                        $modeloTyCInformacionesEspecificas->rutas_imagenes="imagenProducto/placeholder.png";

                        $modeloTyCProducto->cantidades=0;

                        $modeloTyCProducto->nombres=$request->input('nombreProducto');

                        $modeloTyCProducto->fid_subcategorias=$modeloTyCSubCategoria->id;

                        $modeloTyCProducto->ubicaciones=$request->input('ubicacion');

                        if($request->input('ubicacion')=="Colombia"){

                            $modeloTyCProducto->ceudonimosUbicaciones="CO";

                            $modeloTyCInformacionesEspecificas->ceudonimosUbicaciones="CO";

                        }

                        if($request->input('ubicacion')=="Ecuador"){

                            $modeloTyCProducto->ceudonimosUbicaciones="EC";

                            $modeloTyCInformacionesEspecificas->ceudonimosUbicaciones="EC";

                        }

                        $modeloTyCProducto->precios=0;

                        $modeloTyCProducto->save();

                        $modeloTyCInformacionesEspecificas->fid_subcategorias=$modeloTyCSubCategoria->id;

                        $modeloTyCInformacionesEspecificas->descripciones=$request->input('descripcionProducto');

                        $modeloTyCInformacionesEspecificas->codigos=$request->input('codigoProducto');

                        $modeloTyCInformacionesEspecificas->fid_productos=$modeloTyCProducto->id;

                        $modeloTyCInformacionesEspecificas->idCategoria=$modeloTyCCategoria->id;

                        $modeloTyCInformacionesEspecificas->save();

                    }

                }

            }

        }

        return $this->index();

    }

    public function importarProductosMasivamente(Request $request){

       $img1=$request->file('UrlImg1');

        $ruta1=$img1->getClientOriginalName();

        Storage::disk('productoMasivo')->put($ruta1,file_get_contents($img1->getRealPath()));

        $ruta1='productoMasivo/'.$img1->getClientOriginalName();

        $ruta2=$ruta1;

        $files=$request->file('images');

        Excel::load($ruta2, function($archivo) use ($files){

            $resultado=$archivo->get();

            foreach ($resultado as $key => $value) {

                if($value->ubicacion=="Ecuador"){

                    $modeloTyCCategoria = ModeloTyCCategoria::where('nombres',$value->categoria)->where('ceudonimosUbicaciones',"EC")->get()->first();

                    if(!is_null($modeloTyCCategoria)){

                        $modeloTyCSubCategoria= ModeloTyCSubCategoria::where('nombres',$value->sub_categoria)->where('ceudonimosUbicaciones',"EC")->get()->first();

                        if(!is_null($modeloTyCSubCategoria)){
                            
                            if($value->imagen!=null){

                                $bandera=0;

                                foreach ($files as $file) {
                                    
                                    if($value->imagen==$file->getClientOriginalName()){

                                        $bandera=1;

                                        $modeloTyCProducto=new ModeloTyCProducto;

                                        $modeloTyCInformacionesEspecificas=new ModeloTyCInformacionesEspecificas;

                                        $ruta=time().'_'.$file->getClientOriginalName();

                                        Storage::disk('imagenProducto')->put($ruta,file_get_contents($file->getRealPath()));

                                        $ruta1='imagenProducto/'.$ruta;

                                        $modeloTyCInformacionesEspecificas->rutas_imagenes=$ruta1;

                                        $modeloTyCProducto->cantidades=0;

                                        $modeloTyCProducto->nombres=$value->nombre;

                                        $modeloTyCProducto->fid_subcategorias=$modeloTyCSubCategoria->id;

                                        $modeloTyCProducto->ubicaciones=$value->ubicacion;

                                        if($value->ubicacion=="Colombia"){

                                            $modeloTyCProducto->ceudonimosUbicaciones="CO";

                                            $modeloTyCInformacionesEspecificas->ceudonimosUbicaciones="CO";

                                        }

                                        if($value->ubicacion=="Ecuador"){

                                            $modeloTyCProducto->ceudonimosUbicaciones="EC";

                                            $modeloTyCInformacionesEspecificas->ceudonimosUbicaciones="EC";

                                        }

                                        $modeloTyCProducto->precios=0;

                                        $modeloTyCProducto->save();

                                        $modeloTyCInformacionesEspecificas->fid_subcategorias=$modeloTyCSubCategoria->id;

                                        $modeloTyCInformacionesEspecificas->descripciones=$value->descripcion;

                                        $modeloTyCInformacionesEspecificas->codigos=$value->codigo;

                                        $modeloTyCInformacionesEspecificas->fid_productos=$modeloTyCProducto->id;

                                        $modeloTyCInformacionesEspecificas->idCategoria=$modeloTyCCategoria->id;

                                        $modeloTyCInformacionesEspecificas->save();

                                    }

                                }

                                if($bandera==0){

                                    $modeloTyCProducto=new ModeloTyCProducto;

                                    $modeloTyCInformacionesEspecificas=new ModeloTyCInformacionesEspecificas;

                                    $modeloTyCInformacionesEspecificas->rutas_imagenes="imagenProducto/placeholder.png";

                                    $modeloTyCProducto->cantidades=0;

                                    $modeloTyCProducto->nombres=$value->nombre;

                                    $modeloTyCProducto->fid_subcategorias=$modeloTyCSubCategoria->id;

                                    $modeloTyCProducto->ubicaciones=$value->ubicacion;

                                    if($value->ubicacion=="Colombia"){

                                            $modeloTyCProducto->ceudonimosUbicaciones="CO";

                                            $modeloTyCInformacionesEspecificas->ceudonimosUbicaciones="CO";

                                        }

                                        if($value->ubicacion=="Ecuador"){

                                            $modeloTyCProducto->ceudonimosUbicaciones="EC";

                                            $modeloTyCInformacionesEspecificas->ceudonimosUbicaciones="EC";

                                        }

                                    $modeloTyCProducto->precios=0;

                                    $modeloTyCProducto->save();

                                    $modeloTyCInformacionesEspecificas->fid_subcategorias=$modeloTyCSubCategoria->id;

                                    $modeloTyCInformacionesEspecificas->descripciones=$value->descripcion;

                                    $modeloTyCInformacionesEspecificas->codigos=$value->codigo;

                                    $modeloTyCInformacionesEspecificas->fid_productos=$modeloTyCProducto->id;

                                    $modeloTyCInformacionesEspecificas->idCategoria=$modeloTyCCategoria->id;

                                    $modeloTyCInformacionesEspecificas->save();

                                }

                            }else{

                                $modeloTyCProducto=new ModeloTyCProducto;

                                $modeloTyCInformacionesEspecificas=new ModeloTyCInformacionesEspecificas;

                                $modeloTyCInformacionesEspecificas->rutas_imagenes="imagenProducto/placeholder.png";

                                $modeloTyCProducto->cantidades=0;

                                $modeloTyCProducto->nombres=$value->nombre;

                                $modeloTyCProducto->fid_subcategorias=$modeloTyCSubCategoria->id;

                                $modeloTyCProducto->ubicaciones=$value->ubicacion;

                                if($value->ubicacion=="Colombia"){

                                            $modeloTyCProducto->ceudonimosUbicaciones="CO";

                                            $modeloTyCInformacionesEspecificas->ceudonimosUbicaciones="CO";

                                        }

                                        if($value->ubicacion=="Ecuador"){

                                            $modeloTyCProducto->ceudonimosUbicaciones="EC";

                                            $modeloTyCInformacionesEspecificas->ceudonimosUbicaciones="EC";

                                        }

                                $modeloTyCProducto->precios=0;

                                $modeloTyCProducto->save();

                                $modeloTyCInformacionesEspecificas->fid_subcategorias=$modeloTyCSubCategoria->id;

                                $modeloTyCInformacionesEspecificas->descripciones=$value->descripcion;

                                $modeloTyCInformacionesEspecificas->codigos=$value->codigo;

                                $modeloTyCInformacionesEspecificas->fid_productos=$modeloTyCProducto->id;

                                $modeloTyCInformacionesEspecificas->idCategoria=$modeloTyCCategoria->id;

                                $modeloTyCInformacionesEspecificas->save();

                            }
                        }

                    }

                }else{

                    $modeloTyCCategoria = ModeloTyCCategoria::where('nombres',$value->categoria)->where('ceudonimosUbicaciones',"CO")->get()->first();

                    if(!is_null($modeloTyCCategoria)){

                        $modeloTyCSubCategoria= ModeloTyCSubCategoria::where('nombres',$value->sub_categoria)->where('ceudonimosUbicaciones',"CO")->get()->first();

                        if(!is_null($modeloTyCSubCategoria)){

                            
                            if($value->imagen!=null){

                                $bandera=0;

                                foreach ($files as $file) {
                                    
                                    if($value->imagen==$file->getClientOriginalName()){

                                        $bandera=1;

                                        $modeloTyCProducto=new ModeloTyCProducto;

                                        $modeloTyCInformacionesEspecificas=new ModeloTyCInformacionesEspecificas;

                                        $ruta=time().'_'.$file->getClientOriginalName();

                                        Storage::disk('imagenProducto')->put($ruta,file_get_contents($file->getRealPath()));

                                        $ruta1='imagenProducto/'.$ruta;

                                        $modeloTyCInformacionesEspecificas->rutas_imagenes=$ruta1;

                                        $modeloTyCProducto->cantidades=0;

                                        $modeloTyCProducto->nombres=$value->nombre;

                                        $modeloTyCProducto->fid_subcategorias=$modeloTyCSubCategoria->id;

                                        $modeloTyCProducto->ubicaciones=$value->ubicacion;

                                        if($value->ubicacion=="Colombia"){

                                            $modeloTyCProducto->ceudonimosUbicaciones="CO";

                                            $modeloTyCInformacionesEspecificas->ceudonimosUbicaciones="CO";

                                        }

                                        if($value->ubicacion=="Ecuador"){

                                            $modeloTyCProducto->ceudonimosUbicaciones="EC";

                                            $modeloTyCInformacionesEspecificas->ceudonimosUbicaciones="EC";

                                        }

                                        $modeloTyCProducto->precios=0;

                                        $modeloTyCProducto->save();

                                        $modeloTyCInformacionesEspecificas->fid_subcategorias=$modeloTyCSubCategoria->id;

                                        $modeloTyCInformacionesEspecificas->descripciones=$value->descripcion;

                                        $modeloTyCInformacionesEspecificas->codigos=$value->codigo;

                                        $modeloTyCInformacionesEspecificas->fid_productos=$modeloTyCProducto->id;

                                        $modeloTyCInformacionesEspecificas->idCategoria=$modeloTyCCategoria->id;

                                        $modeloTyCInformacionesEspecificas->save();

                                    }

                                }

                                if($bandera==0){

                                    $modeloTyCProducto=new ModeloTyCProducto;

                                    $modeloTyCInformacionesEspecificas=new ModeloTyCInformacionesEspecificas;

                                    $modeloTyCInformacionesEspecificas->rutas_imagenes="imagenProducto/placeholder.png";

                                    $modeloTyCProducto->cantidades=0;

                                    $modeloTyCProducto->nombres=$value->nombre;

                                    $modeloTyCProducto->fid_subcategorias=$modeloTyCSubCategoria->id;

                                    $modeloTyCProducto->ubicaciones=$value->ubicacion;

                                    if($value->ubicacion=="Colombia"){

                                            $modeloTyCProducto->ceudonimosUbicaciones="CO";

                                            $modeloTyCInformacionesEspecificas->ceudonimosUbicaciones="CO";

                                        }

                                        if($value->ubicacion=="Ecuador"){

                                            $modeloTyCProducto->ceudonimosUbicaciones="EC";

                                            $modeloTyCInformacionesEspecificas->ceudonimosUbicaciones="EC";

                                        }

                                    $modeloTyCProducto->precios=0;

                                    $modeloTyCProducto->save();

                                    $modeloTyCInformacionesEspecificas->fid_subcategorias=$modeloTyCSubCategoria->id;

                                    $modeloTyCInformacionesEspecificas->descripciones=$value->descripcion;

                                    $modeloTyCInformacionesEspecificas->codigos=$value->codigo;

                                    $modeloTyCInformacionesEspecificas->fid_productos=$modeloTyCProducto->id;

                                    $modeloTyCInformacionesEspecificas->idCategoria=$modeloTyCCategoria->id;

                                    $modeloTyCInformacionesEspecificas->save();

                                }

                            }else{

                                $modeloTyCProducto=new ModeloTyCProducto;

                                $modeloTyCInformacionesEspecificas=new ModeloTyCInformacionesEspecificas;

                                $modeloTyCInformacionesEspecificas->rutas_imagenes="imagenProducto/placeholder.png";

                                $modeloTyCProducto->cantidades=0;

                                $modeloTyCProducto->nombres=$value->nombre;

                                $modeloTyCProducto->fid_subcategorias=$modeloTyCSubCategoria->id;

                                $modeloTyCProducto->ubicaciones=$value->ubicacion;

                                if($value->ubicacion=="Colombia"){

                                            $modeloTyCProducto->ceudonimosUbicaciones="CO";

                                            $modeloTyCInformacionesEspecificas->ceudonimosUbicaciones="CO";

                                        }

                                        if($value->ubicacion=="Ecuador"){

                                            $modeloTyCProducto->ceudonimosUbicaciones="EC";

                                            $modeloTyCInformacionesEspecificas->ceudonimosUbicaciones="EC";

                                        }

                                $modeloTyCProducto->precios=0;

                                $modeloTyCProducto->save();

                                $modeloTyCInformacionesEspecificas->fid_subcategorias=$modeloTyCSubCategoria->id;

                                $modeloTyCInformacionesEspecificas->descripciones=$value->descripcion;

                                $modeloTyCInformacionesEspecificas->codigos=$value->codigo;

                                $modeloTyCInformacionesEspecificas->fid_productos=$modeloTyCProducto->id;

                                $modeloTyCInformacionesEspecificas->idCategoria=$modeloTyCCategoria->id;

                                $modeloTyCInformacionesEspecificas->save();

                            }
                        }
                    }
                }

            }
        })->get();

        return $this->verImportacionMasiva();

    }


    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

                