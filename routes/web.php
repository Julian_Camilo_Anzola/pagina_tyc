<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleSware group. Now create something great!
|
*/

Route::get('/', 'ControladorCargarPaginaPrincipal@index');

Route::get('/PanelAdministracion', 'ControladorAdministrador@verPanelAdministrador');

Route::get('/AgregarAdministracion', 'ControladorAdministrador@agregarAdministrador');

Route::get('/AgregarCategoria', 'ControladorModificadorClasificadorProducto@VistaAgregarCategoria');

Route::post('/GuardarCategoria', 'ControladorModificadorClasificadorProducto@agregarCategoria');

Route::get('/AgregarProducto', 'ControladorModificadorProducto@index');

Route::post('/GuardarProducto', 'ControladorModificadorProducto@agregarProducto');

Route::get('/VerProductos/{c}', 'ControladorBarraDeBusqueda@cargarProductos');

Route::get('/VerInformacionProductos/{c}', 'ControladorBarraDeBusqueda@mostrarInformacionProducto');

Route::get('/VerSubCategoria/{c}', 'ControladorProducto@verSubCategoria');

Route::get('/VerImportacionProductosMasivamente', 'ControladorModificadorProducto@verImportacionMasiva');

Route::post('/ImportarPrudctosMasivamente', 'ControladorModificadorProducto@importarProductosMasivamente');

Route::get('/VerEliminarAdministrador', 'ControladorAdministrador@eliminarAdministrador');

Route::get('/EliminarAdministrador/{u}', 'ControladorAdministrador@borrarAdministrador');

Route::get('/ListarParaModificarAdministrador', 'ControladorAdministrador@listaModificarAdministrador');

Route::get('/ModificarAdministrador/{u}', 'ControladorAdministrador@modificarAdministrador');

Route::post('/GuardarModificacionAdministrador', 'ControladorAdministrador@guardarModificacionAdministrador');

Route::get('/CambiarContrasenaAdministrador', 'ControladorAdministrador@cambiarContrasenaAdministrador');

Route::post('/GuardarCambiarContrasenaAdministrador', 'ControladorAdministrador@guardarCambiarContrasenaAdministrador');

Route::get('/ListarProductos', 'ControladorProducto@listarProductoEliminar');

Route::get('/EliminarProductos/{u}', 'ControladorProducto@eliminarProducto');

Route::get('/ModificarProductos/{u}', 'ControladorProducto@modificarProducto');

Route::get('/EliminarImagen/{u}', 'ControladorProducto@eliminarImagen');

Route::post('/GuardarModificacionProducto', 'ControladorProducto@guardarModificacionProducto');

Route::post('/BuscarProductoBarraDeBusqueda', 'ControladorBarraDeBusqueda@index');

Route::get('/VerHistoria', 'ControladorCargarPaginaPrincipal@verHistoria');

Route::get('/VerContactos', 'ControladorCargarPaginaPrincipal@verContactos');

Route::get('logout', ['as' => 'logout', 'uses' => 'Auth\AuthController@getLogout']);




/*
Route::get('/ActualizarCorreo', 'ControladorActualizarCorreo@index');

Route::post('/GuardarActualizarCorreo', 'ControladorActualizarCorreo@store');

*/

Route::get('/VerActualizarCorreo', 'ControladorModificadorClasificadorProducto@CorreosActualizados');


/*

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => 'auth'], function () {
    //    Route::get('/link1', function ()    {
//        // Uses Auth Middleware
//    });

    //Please do not remove this if you want adminlte:route and adminlte:link commands to works correctly.
    #adminlte_routes
});*/
