<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\ModeloTyCProducto;

use App\ModeloTyCInformacionesEspecificas;

use App\ModeloTyCCategoria;

use App\ModeloTyCSubCategoria;

use Storage;

class ControladorProducto extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function verSubCategoria($id){

        $modeloTyCCategoriaBusqueda=ModeloTyCCategoria::all();

        $modeloTyCSubCategoria=ModeloTyCSubCategoria::all();

        $modeloTyCSubCategoria2=ModeloTyCSubCategoria::where('fid_categorias',$id)->get();

        return view('VistaSubCategoria',compact('modeloTyCSubCategoria2','modeloTyCCategoriaBusqueda','modeloTyCSubCategoria'));

    }

    public function listarProductoEliminar(){

        if(session('ubicacion')=="EC"){
            
            $modeloTyCCategoriaBusqueda=ModeloTyCCategoria::where('ceudonimosUbicaciones',"EC")->get();

            $modeloTyCSubCategoria=ModeloTyCSubCategoria::where('ceudonimosUbicaciones',"EC")->get();

            $modeloTyCProducto = ModeloTyCProducto::all();

            return view('VistaListarProductosParaEliminar',compact('modeloTyCCategoriaBusqueda','modeloTyCSubCategoria','modeloTyCProducto'));

        }else{

            $modeloTyCCategoriaBusqueda=ModeloTyCCategoria::where('ceudonimosUbicaciones',"CO")->get();

            $modeloTyCSubCategoria=ModeloTyCSubCategoria::where('ceudonimosUbicaciones',"CO")->get();

            $modeloTyCProducto = ModeloTyCProducto::all();

            return view('VistaListarProductosParaEliminar',compact('modeloTyCCategoriaBusqueda','modeloTyCSubCategoria','modeloTyCProducto'));

        }

    }


    public function eliminarProducto($id){

        $modeloTyCProducto = ModeloTyCProducto::where('id',$id)->delete();

        $modeloTyCInformacionesEspecificas = ModeloTyCInformacionesEspecificas::where('fid_productos',$id)->delete();

        return $this->listarProductoEliminar();

    }

    public function modificarProducto($id){

        if(session('ubicacion')=="EC"){

            $modeloTyCCategoriaBusqueda=ModeloTyCCategoria::where('ceudonimosUbicaciones',"CO")->get();

            $modeloTyCSubCategoria=ModeloTyCSubCategoria::where('ceudonimosUbicaciones',"CO")->get();

            $modeloTyCProducto = ModeloTyCProducto::all();

            $modeloTyCInformacionesEspecificas2 = ModeloTyCInformacionesEspecificas::where('fid_productos',$id)->get()->first();

            $modeloTyCProducto2 = ModeloTyCProducto::find($id);

            $modeloTyCSubCategoria2=ModeloTyCSubCategoria::find($modeloTyCProducto2->fid_subcategorias);

            $modeloTyCCategoriaBusqueda2=ModeloTyCCategoria::find($modeloTyCSubCategoria2->fid_categorias);

            return view('VistaModificarProducto',compact('modeloTyCInformacionesEspecificas2','modeloTyCProducto','modeloTyCProducto2','modeloTyCSubCategoria','modeloTyCSubCategoria2','modeloTyCCategoriaBusqueda','modeloTyCCategoriaBusqueda2'));


        }else{

            $modeloTyCCategoriaBusqueda=ModeloTyCCategoria::where('ceudonimosUbicaciones',"CO")->get();

            $modeloTyCSubCategoria=ModeloTyCSubCategoria::where('ceudonimosUbicaciones',"CO")->get();

            $modeloTyCProducto = ModeloTyCProducto::all();

            $modeloTyCInformacionesEspecificas2 = ModeloTyCInformacionesEspecificas::where('fid_productos',$id)->get()->first();

            $modeloTyCProducto2 = ModeloTyCProducto::find($id);

            $modeloTyCSubCategoria2=ModeloTyCSubCategoria::find($modeloTyCProducto2->fid_subcategorias);

            $modeloTyCCategoriaBusqueda2=ModeloTyCCategoria::find($modeloTyCSubCategoria2->fid_categorias);

            return view('VistaModificarProducto',compact('modeloTyCInformacionesEspecificas2','modeloTyCProducto','modeloTyCProducto2','modeloTyCSubCategoria','modeloTyCSubCategoria2','modeloTyCCategoriaBusqueda','modeloTyCCategoriaBusqueda2'));            

        }

    }

    public function eliminarImagen($id){

        $modeloTyCInformacionesEspecificas = ModeloTyCInformacionesEspecificas::find($id);

        $modeloTyCInformacionesEspecificas->rutas_imagenes="imagenProducto/placeholder.png";

        $modeloTyCInformacionesEspecificas->save();

        if($modeloTyCInformacionesEspecificas->rutas_imagenes!="imagenProducto/placeholder.png"){

            unlink(public_path($modeloTyCInformacionesEspecificas->rutas_imagenes));

        }

        return $this->modificarProducto($modeloTyCInformacionesEspecificas->fid_productos);

    }

    public function guardarModificacionProducto(Request $request){

        $modeloTyCProducto = ModeloTyCProducto::find($request->input('idProducto'));

        $modeloTyCInformacionesEspecificas = ModeloTyCInformacionesEspecificas::find($request->input('idInformacionProducto'));

        $modeloTyCCategoria=ModeloTyCCategoria::where('nombres',$request->input('categoria'))->get()->first();

        $modeloTyCSubCategoria=ModeloTyCSubCategoria::where('nombres',$request->input('subCatgoria'))->get()->first();

        if(!is_null($modeloTyCCategoria)&& !is_null($modeloTyCSubCategoria)){

            $modeloTyCProducto->nombres=$request->input('nombreProducto');

            $modeloTyCProducto->ubicaciones=$request->input('ubicacion');

            if($request->input('ubicacion')=="Colombia"){

                $modeloTyCProducto->ceudonimosUbicaciones="CO";

                $modeloTyCInformacionesEspecificas->ceudonimosUbicaciones="CO";

            }

            if($request->input('ubicacion')=="Ecuador"){

                $modeloTyCProducto->ceudonimosUbicaciones="EC";

                $modeloTyCInformacionesEspecificas->ceudonimosUbicaciones="EC";

            }

            $modeloTyCProducto->fid_subcategorias=$modeloTyCSubCategoria->id;

            $modeloTyCProducto->save();

            $modeloTyCInformacionesEspecificas->descripciones=$request->input('descripcionProducto');

            if($request->file('UrlImg2')!=null){

                if($modeloTyCInformacionesEspecificas->rutas_imagenes!="imagenProducto/placeholder.png"){

                    unlink(public_path($modeloTyCInformacionesEspecificas->rutas_imagenes));

                }

                $ruta=time().'_'.$request->file('UrlImg2')->getClientOriginalName();

                Storage::disk('imagenProducto')->put($ruta,file_get_contents($request->file('UrlImg2')->getRealPath()));

                $ruta1='imagenProducto/'.$ruta;

                $modeloTyCInformacionesEspecificas->rutas_imagenes=$ruta1;

            }

            $modeloTyCInformacionesEspecificas->idCategoria=$modeloTyCCategoria->id;

            $modeloTyCInformacionesEspecificas->fid_subcategorias=$modeloTyCSubCategoria->id;

            $modeloTyCInformacionesEspecificas->codigos=$request->input('codigoProducto');

            $modeloTyCInformacionesEspecificas->save();

        }else{

            dd("Tiene que ingresar una categoria valida");

        }

        

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
