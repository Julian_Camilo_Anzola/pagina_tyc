<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\ModeloTyCCategoria;

use App\ModeloTyCSubCategoria;

use App\ModeloActualizarCorreo;

use App\ClientesCorreos;

use Storage;

class ControladorModificadorClasificadorProducto extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function CorreosActualizados(){

        if(session('ubicacion')=="EC"){

            $modeloTyCCategoriaBusqueda=ModeloTyCCategoria::where('ceudonimosUbicaciones',"EC")->get();

            $modeloTyCSubCategoria=ModeloTyCSubCategoria::where('ceudonimosUbicaciones',"EC")->get();

            $modeloActualizarCorreo=ClientesCorreos::all();

            return view('VistaVerCorreosActualizados',compact('modeloTyCCategoriaBusqueda','modeloTyCSubCategoria','modeloActualizarCorreo'));            

        }else{

            $modeloTyCCategoriaBusqueda=ModeloTyCCategoria::where('ceudonimosUbicaciones',"CO")->get();

            $modeloTyCSubCategoria=ModeloTyCSubCategoria::where('ceudonimosUbicaciones',"CO")->get();
            
            $modeloActualizarCorreo=ClientesCorreos::all();

            return view('VistaVerCorreosActualizados',compact('modeloTyCCategoriaBusqueda','modeloTyCSubCategoria','modeloActualizarCorreo'));  

        }

    }

    public function VistaAgregarCategoria(){

        if(session('ubicacion')=="EC"){

            $modeloTyCCategoriaBusqueda=ModeloTyCCategoria::where('ceudonimosUbicaciones',"EC")->get();

            $modeloTyCSubCategoria=ModeloTyCSubCategoria::where('ceudonimosUbicaciones',"EC")->get();

            return view('VistaAgregarCategoria',compact('modeloTyCCategoriaBusqueda','modeloTyCSubCategoria'));            

        }else{

            $modeloTyCCategoriaBusqueda=ModeloTyCCategoria::where('ceudonimosUbicaciones',"CO")->get();

            $modeloTyCSubCategoria=ModeloTyCSubCategoria::where('ceudonimosUbicaciones',"CO")->get();

            return view('VistaAgregarCategoria',compact('modeloTyCCategoriaBusqueda','modeloTyCSubCategoria'));

        }

    }

    public function agregarCategoria(Request $request){

        if($request->input('ubicacion')=="Ecuador"){

            $modeloTyCCategoriaBusqueda=ModeloTyCCategoria::where('nombres',$request->input('categoria'))->where('ceudonimosUbicaciones',"EC")->get()->first();

            $modeloTyCSubCategoria=new ModeloTyCSubCategoria;

            if(is_null($modeloTyCCategoriaBusqueda)){

                $modeloTyCCategoria=new modeloTyCCategoria;

                if($request->file('UrlImg1')!=null){

                    $ruta=time().'_'.$request->file('UrlImg1')->getClientOriginalName();

                    Storage::disk('imagenCategoria')->put($ruta,file_get_contents($request->file('UrlImg1')->getRealPath()));

                    $ruta1='imagenCategoria/'.$ruta;

                    $modeloTyCCategoria->rutas_imagenes=$ruta1;
                    
                }

                if($request->file('UrlImg2')!=null){

                    $ruta2=time().'_'.$request->file('UrlImg2')->getClientOriginalName();

                    Storage::disk('imagenSubCategoria')->put($ruta2,file_get_contents($request->file('UrlImg2')->getRealPath()));

                    $ruta3='imagenSubCategoria/'.$ruta2;

                    $modeloTyCSubCategoria->rutas_imagenes=$ruta3;
                    
                }

                if($request->input('ubicacion')=="Colombia"){

                    $modeloTyCCategoria->ceudonimosUbicaciones="CO";

                    $modeloTyCSubCategoria->ceudonimosUbicaciones="CO";

                }

                if($request->input('ubicacion')=="Ecuador"){

                    $modeloTyCCategoria->ceudonimosUbicaciones="EC";

                    $modeloTyCSubCategoria->ceudonimosUbicaciones="EC";

                }

                $modeloTyCCategoria->nombres=$request->input('categoria');

                $modeloTyCCategoria->save();

                $modeloTyCSubCategoria->nombres=$request->input('subCatgoria');

                $modeloTyCSubCategoria->fid_categorias=$modeloTyCCategoria->id;

                $modeloTyCSubCategoria->save();

            }else{

                if($request->file('UrlImg2')!=null){

                    $ruta2=time().'_'.$request->file('UrlImg2')->getClientOriginalName();

                    Storage::disk('imagenSubCategoria')->put($ruta2,file_get_contents($request->file('UrlImg2')->getRealPath()));

                    $ruta3='imagenSubCategoria/'.$ruta2;

                    $modeloTyCSubCategoria->rutas_imagenes=$ruta3;
                    
                }

                if($request->input('ubicacion')=="Colombia"){

                    $modeloTyCSubCategoria->ceudonimosUbicaciones="CO";

                }

                if($request->input('ubicacion')=="Ecuador"){

                    $modeloTyCSubCategoria->ceudonimosUbicaciones="EC";

                }

                $modeloTyCSubCategoria->nombres=$request->input('subCatgoria');

                $modeloTyCSubCategoria->fid_categorias=$modeloTyCCategoriaBusqueda->id;

                $modeloTyCSubCategoria->save();

            }

        }else{

            $modeloTyCCategoriaBusqueda=ModeloTyCCategoria::where('nombres',$request->input('categoria'))->where('ceudonimosUbicaciones',"CO")->get()->first();

            $modeloTyCSubCategoria=new ModeloTyCSubCategoria;

            if(is_null($modeloTyCCategoriaBusqueda)){

                $modeloTyCCategoria=new modeloTyCCategoria;

                if($request->file('UrlImg1')!=null){

                    $ruta=time().'_'.$request->file('UrlImg1')->getClientOriginalName();

                    Storage::disk('imagenCategoria')->put($ruta,file_get_contents($request->file('UrlImg1')->getRealPath()));

                    $ruta1='imagenCategoria/'.$ruta;

                    $modeloTyCCategoria->rutas_imagenes=$ruta1;
                    
                }

                if($request->file('UrlImg2')!=null){

                    $ruta2=time().'_'.$request->file('UrlImg2')->getClientOriginalName();

                    Storage::disk('imagenSubCategoria')->put($ruta2,file_get_contents($request->file('UrlImg2')->getRealPath()));

                    $ruta3='imagenSubCategoria/'.$ruta2;

                    $modeloTyCSubCategoria->rutas_imagenes=$ruta3;
                    
                }

                if($request->input('ubicacion')=="Colombia"){

                    $modeloTyCCategoria->ceudonimosUbicaciones="CO";

                    $modeloTyCSubCategoria->ceudonimosUbicaciones="CO";

                }

                if($request->input('ubicacion')=="Ecuador"){

                    $modeloTyCCategoria->ceudonimosUbicaciones="EC";

                    $modeloTyCSubCategoria->ceudonimosUbicaciones="EC";

                }

                $modeloTyCCategoria->nombres=$request->input('categoria');

                $modeloTyCCategoria->save();

                $modeloTyCSubCategoria->nombres=$request->input('subCatgoria');

                $modeloTyCSubCategoria->fid_categorias=$modeloTyCCategoria->id;

                $modeloTyCSubCategoria->save();

            }else{

                if($request->file('UrlImg2')!=null){

                    $ruta2=time().'_'.$request->file('UrlImg2')->getClientOriginalName();

                    Storage::disk('imagenSubCategoria')->put($ruta2,file_get_contents($request->file('UrlImg2')->getRealPath()));

                    $ruta3='imagenSubCategoria/'.$ruta2;

                    $modeloTyCSubCategoria->rutas_imagenes=$ruta3;
                    
                }

                if($request->input('ubicacion')=="Colombia"){

                    $modeloTyCSubCategoria->ceudonimosUbicaciones="CO";

                }

                if($request->input('ubicacion')=="Ecuador"){

                    $modeloTyCSubCategoria->ceudonimosUbicaciones="EC";

                }

                $modeloTyCSubCategoria->nombres=$request->input('subCatgoria');

                $modeloTyCSubCategoria->fid_categorias=$modeloTyCCategoriaBusqueda->id;

                $modeloTyCSubCategoria->save();

            }

        }

        return $this->VistaAgregarCategoria();


    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
