<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientesCorreos extends Model
{
    protected $table ='clientescorreos';

    protected $fillable =['Razon_Social','Nit','Direccion_Principal','ciudad','Departamento','Correo','Correo_FE','Telefono_FE','Persona_FE','Telefono'];
}
