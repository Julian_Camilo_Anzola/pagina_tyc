<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\ModeloTyCProducto;

use App\ModeloTyCCategoria;

use App\ModeloTyCSubCategoria;

use App\ModeloTyCInformacionesEspecificas;

class ControladorBarraDeBusqueda extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if(session('ubicacion')=="EC"){

            $modeloTyCCategoriaBusqueda=ModeloTyCCategoria::where('ceudonimosUbicaciones',"EC")->get();

            $modeloTyCSubCategoria=ModeloTyCSubCategoria::where('ceudonimosUbicaciones',"EC")->get();

            $ModeloTyCProducto=ModeloTyCProducto::where('nombres','LIKE','%'.$request->input('busqueda').'%')->where('ceudonimosUbicaciones',"EC")->get();

            $imagenes = array();

            $id = array();

            foreach ($ModeloTyCProducto as $key ) {
                
                $modeloModeloTyCInformacionesEspecificas=ModeloTyCInformacionesEspecificas::where('fid_productos',$key->id)->get()->first();

                if(!is_null($modeloModeloTyCInformacionesEspecificas)){

                    $imagenes[]=$modeloModeloTyCInformacionesEspecificas->rutas_imagenes;

                    $id[]=$key->id;

                }

            }

            $numero=Count($id);

            return view('VistaBarraDeBusquedaProducto',compact('imagenes','ModeloTyCProducto','modeloTyCCategoriaBusqueda','modeloTyCSubCategoria','id','numero'));

        }else{

            $modeloTyCCategoriaBusqueda=ModeloTyCCategoria::where('ceudonimosUbicaciones',"CO")->get();

            $modeloTyCSubCategoria=ModeloTyCSubCategoria::where('ceudonimosUbicaciones',"CO")->get();

            $ModeloTyCProducto=ModeloTyCProducto::where('nombres','LIKE','%'.$request->input('busqueda').'%')->where('ceudonimosUbicaciones',"CO")->get();

            $imagenes = array();

            $id = array();

            foreach ($ModeloTyCProducto as $key ) {
                
                $modeloModeloTyCInformacionesEspecificas=ModeloTyCInformacionesEspecificas::where('fid_productos',$key->id)->get()->first();

                if(!is_null($modeloModeloTyCInformacionesEspecificas)){

                    $imagenes[]=$modeloModeloTyCInformacionesEspecificas->rutas_imagenes;

                    $id[]=$key->id;

                }

            }

            $numero=Count($id);

            return view('VistaBarraDeBusquedaProducto',compact('imagenes','ModeloTyCProducto','modeloTyCCategoriaBusqueda','modeloTyCSubCategoria','id','numero'));

        }
    }

    public function cargarProductos($id)
    {

        if(session('ubicacion')=="EC"){

            $modeloTyCProducto = ModeloTyCProducto::where('fid_subcategorias',$id)->get();

            $modeloTyCInformacionesEspecificas = ModeloTyCInformacionesEspecificas::where('fid_subcategorias',$id)->get();

            $modeloTyCCategoriaBusqueda=ModeloTyCCategoria::where('ceudonimosUbicaciones',"EC")->get();

            $modeloTyCSubCategoria=ModeloTyCSubCategoria::where('ceudonimosUbicaciones',"EC")->get();
            
            return view('VistaCargarProductos',compact('modeloTyCProducto','modeloTyCInformacionesEspecificas','modeloTyCCategoriaBusqueda','modeloTyCSubCategoria'));

        }else{

            $modeloTyCProducto = ModeloTyCProducto::where('fid_subcategorias',$id)->get();

                $modeloTyCInformacionesEspecificas = ModeloTyCInformacionesEspecificas::where('fid_subcategorias',$id)->get();

                $modeloTyCCategoriaBusqueda=ModeloTyCCategoria::where('ceudonimosUbicaciones',"CO")->get();

                $modeloTyCSubCategoria=ModeloTyCSubCategoria::where('ceudonimosUbicaciones',"CO")->get();
            
                return view('VistaCargarProductos',compact('modeloTyCProducto','modeloTyCInformacionesEspecificas','modeloTyCCategoriaBusqueda','modeloTyCSubCategoria'));

        }

    }

    public function mostrarInformacionProducto($id){


        if(session('ubicacion')=="EC"){

            
            $modeloTyCInformacionesEspecificas = ModeloTyCInformacionesEspecificas::where('fid_productos',$id)->get()->first();

            $modeloTyCProducto = ModeloTyCProducto::find($id);

            $modeloTyCCategoriaBusqueda=ModeloTyCCategoria::where('ceudonimosUbicaciones',"EC")->get();

            $modeloTyCSubCategoria=ModeloTyCSubCategoria::where('ceudonimosUbicaciones',"EC")->get();

            return view('VistaCargarInformacionProducto',compact('modeloTyCInformacionesEspecificas','modeloTyCProducto','modeloTyCCategoriaBusqueda','modeloTyCSubCategoria'));

        }else{

            $modeloTyCInformacionesEspecificas = ModeloTyCInformacionesEspecificas::where('fid_productos',$id)->get()->first();

            $modeloTyCProducto = ModeloTyCProducto::find($id);

            $modeloTyCCategoriaBusqueda=ModeloTyCCategoria::where('ceudonimosUbicaciones',"CO")->get();

            $modeloTyCSubCategoria=ModeloTyCSubCategoria::where('ceudonimosUbicaciones',"CO")->get();

            return view('VistaCargarInformacionProducto',compact('modeloTyCInformacionesEspecificas','modeloTyCProducto','modeloTyCCategoriaBusqueda','modeloTyCSubCategoria'));            

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
