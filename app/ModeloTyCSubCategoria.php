<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModeloTyCSubCategoria extends Model
{
    
	protected $table ='tyc_subcategoria';

    protected $fillable =['id','nombres','rutas_imagenes','fid_categorias','ceudonimosUbicaciones'];

    public function tyc_categoria(){

    	return $this->hasOne('App\tyc_categoria');

    }

    public function tyc_productos(){

    	return $this->belongsTo('App\tyc_productos');

    }

    public function tyc_informaciones_especificas(){

    	return $this->belongsTo('App\tyc_informaciones_especificas');

    }

}
