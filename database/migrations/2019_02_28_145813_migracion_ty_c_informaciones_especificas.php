<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MigracionTyCInformacionesEspecificas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tyc_informaciones_especificas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('fid_productos')->nullable($value=false)->unsigned();
            $table->foreign('fid_productos')->references('id')->on('tyc_productos')->onDelete('cascade');
            $table->bigInteger('fid_subcategorias')->nullable($value=false)->unsigned();;
            $table->foreign('fid_subcategorias')->references('id')->on('tyc_subcategoria')->onDelete('cascade');
            $table->string('descripciones')->nullable($value=false);
            $table->string('rutas_imagenes')->nullable($value = false)->collation('utf8_spanish2_ci');
            $table->bigInteger('codigos')->nullable($value=false);
            $table->bigInteger('idCategoria')->nullable($value=false);
            $table->string('ceudonimosUbicaciones')->nullable($value=false)->collation('utf8_spanish2_ci');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tyc_informaciones_especificas');
    }
}
