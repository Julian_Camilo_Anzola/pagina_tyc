<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\ModeloTyCCategoria;

use App\ModeloTyCSubCategoria;

use App\User;

use App\Http\Controllers\Auth\RegisterController;

class ControladorAdministrador extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    

    public function __construct()
    {
        $this->middleware('auth');
    }

    
    public function index()
    {
        //hola jajjaaj
    }

    public function verPanelAdministrador(){

        if(session('ubicacion')=="EC"){

            $modeloTyCCategoriaBusqueda=ModeloTyCCategoria::where('ceudonimosUbicaciones',"EC")->get();

            $modeloTyCSubCategoria=ModeloTyCSubCategoria::where('ceudonimosUbicaciones',"EC")->get();

            return view('VistaPanelAdministracion',compact('modeloTyCCategoriaBusqueda','modeloTyCSubCategoria'));            

        }else{

            $modeloTyCCategoriaBusqueda=ModeloTyCCategoria::where('ceudonimosUbicaciones',"CO")->get();

            $modeloTyCSubCategoria=ModeloTyCSubCategoria::where('ceudonimosUbicaciones',"CO")->get();

            return view('VistaPanelAdministracion',compact('modeloTyCCategoriaBusqueda','modeloTyCSubCategoria'));

        }

    }

    public function agregarAdministrador(){

        if(session('ubicacion')=="EC"){

            $modeloTyCCategoriaBusqueda=ModeloTyCCategoria::where('ceudonimosUbicaciones',"EC")->get();

            $modeloTyCSubCategoria=ModeloTyCSubCategoria::where('ceudonimosUbicaciones',"EC")->get();

            return view('VistaAgregarAdministrador',compact('modeloTyCCategoriaBusqueda','modeloTyCSubCategoria'));            

        }else{

            $modeloTyCCategoriaBusqueda=ModeloTyCCategoria::where('ceudonimosUbicaciones',"CO")->get();

            $modeloTyCSubCategoria=ModeloTyCSubCategoria::where('ceudonimosUbicaciones',"CO")->get();

            return view('VistaAgregarAdministrador',compact('modeloTyCCategoriaBusqueda','modeloTyCSubCategoria'));

        }
    }

    public function eliminarAdministrador(){

        if(session('ubicacion')=="EC"){

            $modeloTyCCategoriaBusqueda=ModeloTyCCategoria::where('ceudonimosUbicaciones',"EC")->get();

            $modeloTyCSubCategoria=ModeloTyCSubCategoria::where('ceudonimosUbicaciones',"EC")->get();

            $modeloUser=User::all();

            return view('VistaEliminarAdministrador',compact('modeloTyCCategoriaBusqueda','modeloTyCSubCategoria','modeloUser'));            

        }else{

            $modeloTyCCategoriaBusqueda=ModeloTyCCategoria::where('ceudonimosUbicaciones',"CO")->get();

            $modeloTyCSubCategoria=ModeloTyCSubCategoria::where('ceudonimosUbicaciones',"CO")->get();

            $modeloUser=User::all();

            return view('VistaEliminarAdministrador',compact('modeloTyCCategoriaBusqueda','modeloTyCSubCategoria','modeloUser'));

        }
    }

    public function borrarAdministrador($id){

        $modeloUser=User::where('id',$id)->delete();

        return $this->eliminarAdministrador();

    }

    public function listaModificarAdministrador(){

        if(session('ubicacion')=="EC"){

            $modeloTyCCategoriaBusqueda=ModeloTyCCategoria::where('ceudonimosUbicaciones',"EC")->get();

            $modeloTyCSubCategoria=ModeloTyCSubCategoria::where('ceudonimosUbicaciones',"EC")->get();

            $modeloUser=User::all();

            return view('VistaListarAdministradoresParaModificar',compact('modeloTyCCategoriaBusqueda','modeloTyCSubCategoria','modeloUser'));            

        }else{

            $modeloTyCCategoriaBusqueda=ModeloTyCCategoria::where('ceudonimosUbicaciones',"CO")->get();

            $modeloTyCSubCategoria=ModeloTyCSubCategoria::where('ceudonimosUbicaciones',"CO")->get();

            $modeloUser=User::all();

            return view('VistaListarAdministradoresParaModificar',compact('modeloTyCCategoriaBusqueda','modeloTyCSubCategoria','modeloUser'));

        }

    }

    public function modificarAdministrador($id){

        if(session('ubicacion')=="EC"){

            $modeloTyCCategoriaBusqueda=ModeloTyCCategoria::where('ceudonimosUbicaciones',"EC")->get();

            $modeloTyCSubCategoria=ModeloTyCSubCategoria::where('ceudonimosUbicaciones',"EC")->get();

            $modeloUser=User::where('id',$id)->get()->first();

            return view('VistaModificarAdministrador',compact('modeloTyCCategoriaBusqueda','modeloTyCSubCategoria','modeloUser'));            

        }else{

            $modeloTyCCategoriaBusqueda=ModeloTyCCategoria::where('ceudonimosUbicaciones',"CO")->get();

            $modeloTyCSubCategoria=ModeloTyCSubCategoria::where('ceudonimosUbicaciones',"CO")->get();

            $modeloUser=User::where('id',$id)->get()->first();

            return view('VistaModificarAdministrador',compact('modeloTyCCategoriaBusqueda','modeloTyCSubCategoria','modeloUser'));

        }
    }

    public function guardarModificacionAdministrador(Request $request){

        $modeloUser=User::find($request->input('id'));

        if((!is_null($modeloUser))&&(!is_null($request->input('name')))&&(!is_null($request->input('email')))){

            $modeloUser->name=$request->input('name');

            $modeloUser->email=$request->input('email');

            $modeloUser->save();

        }else{

            dd(" T Y C AMERICA");

        }

        return $this->listaModificarAdministrador();

    }

    public function cambiarContrasenaAdministrador(){

        if(session('ubicacion')=="EC"){

            $modeloTyCCategoriaBusqueda=ModeloTyCCategoria::where('ceudonimosUbicaciones',"EC")->get();

            $modeloTyCSubCategoria=ModeloTyCSubCategoria::where('ceudonimosUbicaciones',"EC")->get();

            return view('VistaCambiarContrasena',compact('modeloTyCCategoriaBusqueda','modeloTyCSubCategoria'));            

        }else{

            $modeloTyCCategoriaBusqueda=ModeloTyCCategoria::where('ceudonimosUbicaciones',"CO")->get();

            $modeloTyCSubCategoria=ModeloTyCSubCategoria::where('ceudonimosUbicaciones',"CO")->get();

            return view('VistaCambiarContrasena',compact('modeloTyCCategoriaBusqueda','modeloTyCSubCategoria'));

        }

    }

    public function guardarCambiarContrasenaAdministrador(Request $request){

        $modeloUser=User::where('email',$request->input('email'))->where('name',$request->input('nombre'))->delete();

        if($modeloUser==0){

            $copia = array();

            $copia['name']=$request->input('nombre');

            $copia['email']=$request->input('email');   

            $copia['password']=$request->input('password_confirmation');         

            $ControllerRegisterController=new RegisterController();

            return $ControllerRegisterController->intermediario($copia);

        }else{


        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
