
@extends('adminlte::layouts.app')

@section('htmlheader_title')

Vista Principal
	
@endsection


@section('main-content')

<div style="text-align: right;">
  
  <input type=image src="{{ asset('css/Mi cuenta.png') }}" width="80" height="25" style="text-align: right;" onclick="location.href='{{ url('/PanelAdministracion/') }}'">

</div>
  
<div style="text-align: left;">
  <a href="{{ url('/') }}" >
                        <img src="{{ asset('css/LOGO T&C.png') }}" onclick="location.href='{{ url('/') }}'" style="width: 230px; height: 70px; text-align: center;text-align: center;" />
  </a>

</div>

<br>
<br>

<nav class="navbar navbar-default navbar-inverse "  style="background-color: rgba(230, 231,232) !important; border: rgba(230, 231,232) 0.5px solid;">



                <div class="container-fluid" style="border: rgba(230, 231,232) 0.5px solid;">
                    <div class="navbar-header" style="border: rgba(230, 231,232) 0.5px solid; text-align: center;">
 
                            <buttom class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" style="border: rgba(230, 231,232) 0.5px solid;">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>                               
                            </buttom>
                            
                                

                    </div>

                </div>

                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                        <ul class="nav navbar-nav">

                <!--
                <li class="dropdown messages-menu">
                    
                    <a href="{{ url('/PanelAdministracion') }}" >
                        Administrador
                    </a>
                    
                </li>-->

            <!--
              <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li>
              <li><a href="#">Link</a></li>-->
              <li class="dropdown">
                <a  href="#" class="dropdown-toggle" data-toggle="dropdown" style="background-color: rgba(230, 231,232) !important; border: rgba(230, 231,232) 0.5px solid;color:#000000;font-size: 120%;" onclick="location.href='{{ url('/') }}'">INICIO</span></a>
              </li>
              <li  class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="background-color: rgba(230, 231,232) !important; border: rgba(230, 231,232) 0.5px solid;color:#000000;font-size: 120%;" onclick="location.href='{{ url('/VerHistoria') }}'">HISTORIA</span></a>
              </li>
              
              <!--
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="background-color: rgba(230, 231,232) !important; border: rgba(230, 231,232) 0.5px solid;color:#000000;font-size: 120%;">Vision</a>
              </li>-->

              @foreach ($modeloTyCCategoriaBusqueda as $t )

              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="background-color: rgba(230, 231,232) !important; border: rgba(230, 231,232) 0.5px solid; color:#000000;font-size: 120%;">{{$t->nombres}} <span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">

                    @foreach ($modeloTyCSubCategoria as $c )

                        @if($c->fid_categorias==$t->id)

                            <li><a href="#" onclick="location.href='{{ url('/VerProductos/'.$c->id.'/') }}'" style="color: color:#000000;font-size: 120%;">{{$c->nombres}}</a></li>


                        @endif
                     
                    @endforeach

                </ul>
              </li>

              @endforeach
              <!--
              <li class="dropdown">
                <a  href="#" class="dropdown-toggle" data-toggle="dropdown" style="background-color: rgba(230, 231,232) !important; border: rgba(230, 231,232) 0.5px solid;color:#000000;font-size: 120%;" onclick="location.href='{{ url('/ActualizarCorreo') }}'">CLIENTE</span></a>
              </li>
            -->
              <li class="dropdown">
                <a  href="#" class="dropdown-toggle" data-toggle="dropdown" style="background-color: rgba(230, 231,232) !important; border: rgba(230, 231,232) 0.5px solid;color:#000000;font-size: 120%;" onclick="location.href='{{ url('/VerContactos') }}'">CONTACTOS</span></a>
              </li>
            </ul>
            

            <form action="{{ url('/BuscarProductoBarraDeBusqueda') }}" method="post" class="form-inline my-2 my-lg-0" style="text-align: right;">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input class="form-control mr-sm-2" type="search" name="busqueda" id="busqueda" placeholder="¿Que estas buscando?" aria-label="Search" style="width: 10%;text-align: center;">
                            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar</button>
                          </form>
                    </div>
                
            </nav>



<br>
<br>




<br>
<div class="login-logo">
  <b>ACTUALIZAR DATOS</b>
</div>

<body class="hold-transition login-page">
  <div id="app">
        <div class="login-box">
<center><div class="row">
<div class="btn-group-vertical">
    <center><form action="{{ url('/GuardarActualizarCorreo') }}" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group has-feedback">
                        <label style="color:#000000;font-size: 120%;">Razon Social</label>

                        <input type="text" class="form-control" name="Razon" required autofocus/>

                        <br></br>
                        <label style="color:#000000;font-size: 120%;">Nit</label>
                        <input type="number" class="form-control"  name="Nit" required autofocus/>

                        <br></br>
                        <label style="color:#000000;font-size: 120%;">Digito de verificacion</label>
                        <input type="number" class="form-control"  name="NumeroIdentificacion" autofocus/>
                        
                        <br></br>
                        <label style="color:#000000;font-size: 120%;">Dirección principal</label>
                        <input type="text" class="form-control"  name="Dirección" required autofocus/>

                        <br></br>
                        <label style="color:#000000;font-size: 120%;">Ciudad</label>

                        <input type="text" class="form-control"  name="Ciudad" required autofocus/>

                        <br></br>
                        <label style="color:#000000;font-size: 120%;">Departamento</label>

                        <input type="text" class="form-control"  name="Departamento" required autofocus/>

                        <br></br>
                        <label style="color:#000000;font-size: 120%;">Teléfono</label>

                        <input type="text" class="form-control"  name="telefono22" required autofocus/>

                        <br></br>
                        <label style="color:#000000;font-size: 120%;">Dirección de correo electrónico</label>
                        <input type="email" class="form-control"  name="Correo" required autofocus/>
                        
                        <br></br>
                        <label style="color:#000000;font-size: 120%;">Dirección de correo electrónico (Factura Electronica)</label>
                        <input type="email" class="form-control"  name="correo2" required autofocus/>

                        <br></br>
                        <label style="color:#000000;font-size: 120%;">Teléfono área confirmacion (Factura Electronica)</label>
                        <input type="text" class="form-control"  name="Teléfono" required autofocus/>

                        <br></br>
                        <label style="color:#000000;font-size: 120%;">Nombre completo de la persona autorizada de recibir la Factura Electronica</label>
                        <input type="text" class="form-control"  name="Persona" required autofocus/>
                        
                    </div>
                        <div class="col-xs-4 col-xs-push-1" style="width: 85%;">
                            <button type="submit" class="btn" style="text-align: center;">Guardar</button>
                        </div>
                </form> </center>
    </div>
</div></center>
</div>
</div>
</body>

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
@endsection