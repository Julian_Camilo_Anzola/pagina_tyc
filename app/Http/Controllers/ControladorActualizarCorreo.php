<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\ModeloTyCCategoria;

use App\ModeloTyCSubCategoria;

use App\ModeloTyCProducto;

use App\ModeloTyCInformacionesEspecificas;

use App\ModeloActualizarCorreo;

use App\ClientesCorreos;


class ControladorActualizarCorreo extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if(session('ubicacion')=="EC"){

            $modeloTyCCategoriaBusqueda=ModeloTyCCategoria::where('ceudonimosUbicaciones',"EC")->get();

            $modeloTyCSubCategoria=ModeloTyCSubCategoria::where('ceudonimosUbicaciones',"EC")->get();

            return view('VistaActualizarCorreoClientes',compact('modeloTyCCategoriaBusqueda','modeloTyCSubCategoria'));            

        }else{

            $modeloTyCCategoriaBusqueda=ModeloTyCCategoria::where('ceudonimosUbicaciones',"CO")->get();

            $modeloTyCSubCategoria=ModeloTyCSubCategoria::where('ceudonimosUbicaciones',"CO")->get();

            return view('VistaActualizarCorreoClientes',compact('modeloTyCCategoriaBusqueda','modeloTyCSubCategoria'));

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if(!is_null($request->input('NumeroIdentificacion'))){

            $nit=$request->input('Nit')." - ".$request->input('NumeroIdentificacion');

            $ModeloClientesCorreos=new ClientesCorreos;

            $ModeloClientesCorreos->Razon_Social=$request->input('Razon');

            $ModeloClientesCorreos->Nit=$nit;

            $ModeloClientesCorreos->Direccion_Principal=$request->input('Dirección');

            $ModeloClientesCorreos->ciudad=$request->input('Ciudad');

            $ModeloClientesCorreos->Departamento=$request->input('Departamento');

            $ModeloClientesCorreos->Telefono=$request->input('telefono22');

            $ModeloClientesCorreos->Correo=$request->input('Correo');

            $ModeloClientesCorreos->Correo_FE=$request->input('correo2');

            $ModeloClientesCorreos->Telefono_FE=$request->input('Teléfono');

            $ModeloClientesCorreos->Persona_FE=$request->input('Persona');

            $ModeloClientesCorreos->save();

            $ControllerControladorCargarPaginaPrincipal=new ControladorCargarPaginaPrincipal;

            return $ControllerControladorCargarPaginaPrincipal->index();

        }else{

            $ModeloClientesCorreos=new ClientesCorreos;

            $ModeloClientesCorreos->Razon_Social=$request->input('Razon');

            $ModeloClientesCorreos->Nit=$request->input('Nit');

            $ModeloClientesCorreos->Direccion_Principal=$request->input('Dirección');

            $ModeloClientesCorreos->ciudad=$request->input('Ciudad');

            $ModeloClientesCorreos->Departamento=$request->input('Departamento');

            $ModeloClientesCorreos->Telefono=$request->input('telefono22');

            $ModeloClientesCorreos->Correo=$request->input('Correo');

            $ModeloClientesCorreos->Correo_FE=$request->input('correo2');

            $ModeloClientesCorreos->Telefono_FE=$request->input('Teléfono');

            $ModeloClientesCorreos->Persona_FE=$request->input('Persona');

            $ModeloClientesCorreos->save();

            $ControllerControladorCargarPaginaPrincipal=new ControladorCargarPaginaPrincipal;

            return $ControllerControladorCargarPaginaPrincipal->index();


        }

        echo "<script type='text/javascript'>alert('Se almacenaron sus datos');window.close();</script>";
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
