@extends('adminlte::layouts.app')

@section('htmlheader_title')

Vista Principal
  
@endsection


@section('main-content')
<div style="text-align: right;">
  
  <input type=image src="{{ asset('css/Mi cuenta.png') }}" width="80" height="30" style="text-align: right;" onclick="location.href='{{ url('/PanelAdministracion/') }}'">

</div>
  
<div style="text-align: left;">
  <a href="{{ url('/') }}" >
                        <img src="{{ asset('css/LOGO T&C.png') }}" onclick="location.href='{{ url('/') }}'" style="width: 230px; height: 70px; text-align: center;text-align: center;" />
  </a>

</div>

<br>
<br>

<nav class="navbar navbar-default navbar-inverse "  style="background-color: rgba(230, 231,232) !important; border: rgba(230, 231,232) 0.5px solid;">



                <div class="container-fluid" style="border: rgba(230, 231,232) 0.5px solid;">
                    <div class="navbar-header" style="border: rgba(230, 231,232) 0.5px solid; text-align: center;">
 
                            <buttom class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" style="border: rgba(230, 231,232) 0.5px solid;">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>                               
                            </buttom>
                            
                                

                    </div>

                </div>

                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                        <ul class="nav navbar-nav">

                <!--
                <li class="dropdown messages-menu">
                    
                    <a href="{{ url('/PanelAdministracion') }}" >
                        Administrador
                    </a>
                    
                </li>-->

            <!--
              <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li>
              <li><a href="#">Link</a></li>-->
              
              <li class="dropdown">
                <a  href="#" class="dropdown-toggle" data-toggle="dropdown" style="background-color: rgba(230, 231,232) !important; border: rgba(230, 231,232) 0.5px solid;color:#000000;font-size: 120%;" onclick="location.href='{{ url('/') }}'">INICIO</span></a>
              </li>
              <li  class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="background-color: rgba(230, 231,232) !important; border: rgba(230, 231,232) 0.5px solid;color:#000000;font-size: 120%;" onclick="location.href='{{ url('/VerHistoria') }}'">HISTORIA</span></a>
              </li>
              
              <!--
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="background-color: rgba(230, 231,232) !important; border: rgba(230, 231,232) 0.5px solid;color:#000000;font-size: 120%;">Vision</a>
              </li>-->

              @foreach ($modeloTyCCategoriaBusqueda as $t )

              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="background-color: rgba(230, 231,232) !important; border: rgba(230, 231,232) 0.5px solid; color:#000000;font-size: 120%;">{{$t->nombres}} <span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">

                    @foreach ($modeloTyCSubCategoria as $c )

                        @if($c->fid_categorias==$t->id)

                            <li><a href="#" onclick="location.href='{{ url('/VerProductos/'.$c->id.'/') }}'" style="color: color:#000000;font-size: 120%;">{{$c->nombres}}</a></li>


                        @endif
                     
                    @endforeach

                </ul>
              </li>

              @endforeach
              <!--
              <li class="dropdown">
                <a  href="#" class="dropdown-toggle" data-toggle="dropdown" style="background-color: rgba(230, 231,232) !important; border: rgba(230, 231,232) 0.5px solid;color:#000000;font-size: 120%;" onclick="location.href='{{ url('/ActualizarCorreo') }}'">CLIENTE</span></a>
              </li>-->
              <li class="dropdown">
                <a  href="#" class="dropdown-toggle" data-toggle="dropdown" style="background-color: rgba(230, 231,232) !important; border: rgba(230, 231,232) 0.5px solid;color:#000000;font-size: 120%;" onclick="location.href='{{ url('/VerContactos') }}'">CONTACTOS</span></a>
              </li>
            </ul>
            

            <form action="{{ url('/BuscarProductoBarraDeBusqueda') }}" method="post" class="form-inline my-2 my-lg-0" style="text-align: right;">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input class="form-control mr-sm-2" type="search" name="busqueda" id="busqueda" placeholder="¿Que estas buscando?" aria-label="Search" style="width: 10%;text-align: center;">
                            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar</button>
                          </form>
                    </div>
                
            </nav>



<br>
<br>

<div class="row" style="width: 100%; height: 100%;">
  <div class="col-md-12" style="width: 100%; height: 100%;">

    <div id="mdb-lightbox-ui"></div>

    <div class="mdb-lightbox no-margin">

           
                    <figure class="col-md-4">

                    	<img src="{{ asset($modeloTyCInformacionesEspecificas->rutas_imagenes) }}" class="img-fluid" style="width: 45%; height: 45%;margin: 0.1%; " />

                    	
                      
                    </figure>

                    <figure class="col-md-4">

                    		<h1 ALIGN="center" style="font-size: 150%; font-family: Arial; text-align: left;" ><strong>{{$modeloTyCProducto->nombres}}</strong></h1>

                        <h1 ALIGN="center" style="font-size: 150%; font-family: Arial; text-align: left;" ><strong>Codigo: {{$modeloTyCInformacionesEspecificas->codigos}}</strong></h1>

                        <hr style="color:#000000;"/>

                    		<br>
                      
                        	<p ALIGN="justify" style="font-size: 150%; font-family: Arial;" ><strong>{{$modeloTyCInformacionesEspecificas->descripciones}}</strong></p>

                        	<h1 ALIGN="justify" style="font-size: 150%; font-family: Arial;" ><strong>Ubicacion del producto: {{$modeloTyCProducto->ubicaciones}}</strong></h1>                        
                        
                    	</figure>


    </div>

  </div>
</div>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
@endsection