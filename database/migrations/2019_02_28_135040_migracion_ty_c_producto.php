<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MigracionTyCProducto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tyc_productos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('cantidades')->nullable($value=false);
            $table->string('nombres')->nullable($value = false)->collation('utf8_spanish2_ci');
            $table->bigInteger('fid_subcategorias')->nullable($value=false)->unsigned();;
            $table->foreign('fid_subcategorias')->references('id')->on('tyc_subcategoria')->onDelete('cascade');
            $table->string('ubicaciones')->nullable($value=false)->collation('utf8_spanish2_ci');
            $table->string('ceudonimosUbicaciones')->nullable($value=false)->collation('utf8_spanish2_ci');
            $table->bigInteger('precios')->nullable($value=false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tyc_productos');
    }
}
