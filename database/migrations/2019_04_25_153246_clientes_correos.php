<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ClientesCorreos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientescorreos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('Razon_Social')->nullable($value=false)->collation('utf8_spanish2_ci');
            $table->string('Nit')->nullable($value=false)->collation('utf8_spanish2_ci');
            $table->string('Direccion_Principal')->nullable($value=false)->collation('utf8_spanish2_ci');
            $table->string('ciudad')->nullable($value=false)->collation('utf8_spanish2_ci');
            $table->string('Departamento')->nullable($value=false)->collation('utf8_spanish2_ci');
            $table->string('Correo')->nullable($value=false)->collation('utf8_spanish2_ci');
            $table->string('Correo_FE')->nullable($value=false)->collation('utf8_spanish2_ci');
            $table->string('Telefono_FE')->nullable($value=false)->collation('utf8_spanish2_ci');
            $table->string('Persona_FE')->nullable($value=false)->collation('utf8_spanish2_ci');
            $table->string('Telefono')->nullable($value=false)->collation('utf8_spanish2_ci');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientescorreos');
    }
}
