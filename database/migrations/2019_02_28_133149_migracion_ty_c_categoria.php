<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MigracionTyCCategoria extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tyc_categoria', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();;
            $table->string('nombres')->nullable($value = false)->collation('utf8_spanish2_ci');
            $table->string('rutas_imagenes')->nullable($value=true)->collation('utf8_spanish2_ci');
            $table->string('ceudonimosUbicaciones')->nullable($value=false)->collation('utf8_spanish2_ci');
            $table->unique('id');
            $table->timestamps();
        });

        DB::table('tyc_categoria')->insert(
            array(
                'nombres' => 'PROMOCION',
                'rutas_imagenes' => 'No hay ruta',
                'ceudonimosUbicaciones' => 'CO',
                'id' => '1'
            )
        );
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tyc_categoria');
    }
}
