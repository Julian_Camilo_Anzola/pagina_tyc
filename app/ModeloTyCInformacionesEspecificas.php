<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModeloTyCInformacionesEspecificas extends Model
{
    
	protected $table ='tyc_informaciones_especificas';

    protected $fillable =['id','fid_productos','descripciones','rutas_imagenes','codigos','fid_subcategorias','idCategoria','ceudonimosUbicaciones'];

    public function tyc_productos(){

    	return $this->hasOne('App\tyc_productos');

    }

}
